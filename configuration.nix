# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [
      ./modules/base.nix
      ./modules/network.nix
      ./modules/sops.nix
    ];

  nix = {
    useSandbox = false;
    extraOptions = "experimental-features = nix-command flakes";
    package = pkgs.nixFlakes;
  };

  time.timeZone = "Europe/Berlin";
  system.stateVersion = "20.11";

}

