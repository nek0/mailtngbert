{ config, lib, pkgs, buildVM, ... }:

{
  assertions = [
    { assertion = buildVM; message = "this module may only be used when building a VM!"; }
  ];

  users.users.root.hashedPassword = "";
  users.mutableUsers = false;
  
  networking.useDHCP = lib.mkForce false;
  networking.interfaces = lib.mkForce {
    eth0.useDHCP = true;
  };
  networking.defaultGateway = lib.mkForce null;

  #sops.defaultSopsFile = lib.mkForce ../secrets.test.yaml;
  #sops.age.sshKeyPaths = lib.mkForce [];
  #sops.gnupg.sshKeyPaths = lib.mkForce [];
  #sops.age.keyFile = lib.mkForce "${../keys/test.age}";
  #sops.age.generateKey = lib.mkForce false;


  # Set VM disk size (in MB)
  virtualisation.diskSize = 2048;

  # Set VM ram amount (in MB)
  virtualisation.memorySize = 1024;
}
