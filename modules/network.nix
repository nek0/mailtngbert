{pkgs, lib, ...} : {
  networking = {
    interfaces.eth0.ipv4.addresses = [
      { 
        address = "172.20.73.42"; 
        prefixLength = 26; 
      }
    ];
    defaultGateway = "172.20.73.1";
    nameservers = [ 
      "172.20.73.8"
      "9.9.9.9"
    ];
    hostName = lib.mkDefault "mailtngbert";
    useDHCP = false;
    interfaces.eth0.useDHCP = false;
  	firewall.allowedTCPPorts = [ 22 ];
  };
}

