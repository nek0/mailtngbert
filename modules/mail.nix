{ config, pkgs, ... }:
let release = "nixos-21.11";
in {
  mailserver = {
    enable = true;
    fqdn = "mail.example.com";
    domains = [ "example.com" "example2.com" ];
    loginAccounts = {
      "user1@example.com" = {
        # nix run nixpkgs.apacheHttpd -c htpasswd -nbB "" "super secret password" | cut -d: -f2 > /hashed/password/file/location
        hashedPasswordFile = "/hashed/password/file/location";

        aliases = [
        ];
      };
    };
  };
}

