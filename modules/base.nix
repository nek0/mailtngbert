{ pkgs, lib, config, ... }:

{
  _module.args.buildVM = false;

  users.users.root = {
    openssh.authorizedKeys.keyFiles = [
      ../keys/ssh/revol-xut
      ../keys/ssh/nek0
      ../keys/ssh/j03
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    atop
    fish
    git
    htop
    tmux
    vim_configurable
    wget 
    neovim
];

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;
  services.openssh.permitRootLogin = "without-password";
}
