{pkgs, config, lib, ... }: {

  # relevant configuration for running this as lxc proxmox container
  boot.isContainer = true;
  boot.loader.initScript.enable = true;
}
