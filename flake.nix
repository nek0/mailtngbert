{
  description = "Testing Nix Email-Setup for C3D2";

  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
    sops-nix.url = github:Mic92/sops-nix;
    sops-nix.inputs.nixpkgs.follows = "nixpkgs";
    simple-nixos-mailserver.url = "gitlab:simple-nixos-mailserver/nixos-mailserver/nixos-21.11";
  };

  outputs = { self, nixpkgs, sops-nix, simple-nixos-mailserver, ... }@inputs: {
    defaultPackage."x86_64-linux" = (nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = { inherit inputs; };
      modules = [
        simple-nixos-mailserver.nixosModule
        ./configuration.nix

        (_: { _module.args.buildVM = true; })
        "${nixpkgs}/nixos/modules/virtualisation/qemu-vm.nix"
        ./modules/vm.nix
      ];
    }).config.system.build.vm;

    nixosConfigurations.mailtngbert = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      specialArgs = { inherit inputs; };
      modules = [
        simple-nixos-mailserver.nixosModule
        ./configuration.nix
        ./modules/proxmox.nix
      ];
    };
  };
}
