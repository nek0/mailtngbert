The next generation Mailbert
-----------------------------

The Nix Mail setup to rule them all.

## Useful Links

- <https://nixos-mailserver.readthedocs.io/_/downloads/en/latest/pdf/>
- <https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/>

## TODO-List

- [ ] people should add their puplic keys
- [ ] replace keys/test.age
- [ ] setup system and init sops

## Testing Locally

Using Qemu as supervisor.
``
    $ nix build
``
